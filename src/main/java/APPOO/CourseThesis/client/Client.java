package APPOO.CourseThesis.client;

import APPOO.CourseThesis.tools.Constants;
import APPOO.CourseThesis.ui.ChatForm;
import APPOO.CourseThesis.ui.LoginForm;

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dennis on 22-May-17.
 **/
public class Client {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private User user;
    private ClientMessageReceiver clientMessageReceiver;

    public Client(String host, String nickname) {

        user = new User(nickname, host, Constants.Server.PORT);
    }

    public void connectClient() {
        try {
            clientSocket = new Socket(user.getHost(), Constants.Server.PORT);

            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            clientMessageReceiver = new ClientMessageReceiver(in);
            clientMessageReceiver.start();

        } catch (IOException e) {
            LoginForm.infoBox(LoginForm.loginPanel,"Please try later! Server is offline!","Offline", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    }

    public void sendMessage(String message) {
        out.println(Constants.Client.MESSAGE_COMMAND + Constants.Client.SEPARATOR + message);
    }

    public void sendPMessage(String user, String message) {
        out.println(Constants.Client.PRIVATE_MESSAGE_COMMAND + Constants.Client.SEPARATOR + user + Constants.Client.SEPARATOR + message);
    }

    public void sendExitMessage() {
        out.println(Constants.Client.EXIT_COMMAND + Constants.Client.SEPARATOR);

        try {
            in.close();
            clientMessageReceiver.interrupt();
            out.close();
            clientSocket.close();
        }
        catch (IOException e) {

        }

        System.exit(0);
    }

    private String getOnlineUsers() {


        out.println(Constants.Client.LIST_COMMAND + Constants.Client.SEPARATOR);

        //TODO it's not cool, rewrite me please!
        while (clientMessageReceiver.noMessages()) {}

        return clientMessageReceiver.getMessage();
    }

    public boolean validateUser(String nickname) {

        out.println(Constants.Client.NEW_USER + Constants.Client.SEPARATOR + nickname);

        //TODO it's not cool, rewrite me please!
        while (clientMessageReceiver.noMessages()) {}

        String response = clientMessageReceiver.getMessage();
        List<String> serverPackage = Arrays.asList(response.split(Constants.Client.SEPARATOR));

        return Boolean.valueOf(serverPackage.get(1));
    }

    public boolean validatePMUser(String nickname) {

        out.println(Constants.Client.PRIVATE_MESSAGE_COMMAND + Constants.Client.SEPARATOR + nickname);

        //TODO it's not cool, rewrite me please!
        while (clientMessageReceiver.noMessages()) {}

        String response = clientMessageReceiver.getMessage();

        List<String> serverPackage = Arrays.asList(response.split(Constants.Client.SEPARATOR));

        return Boolean.valueOf(serverPackage.get(1));
    }

    public static boolean isValid(String nickname) {

        return nickname.matches("[A-Za-z0-9]+");
    }

    public User getUser() {
        return user;
    }

    public void outputOnlineUsers() {
        ChatForm.chat.append("Online users:\n");
        List<String> usersList = Arrays.asList(getOnlineUsers().split(Constants.Client.SEPARATOR));

        for (String user: usersList) {
            if (!user.isEmpty() && !user.equals(Constants.Client.LIST_COMMAND)) {
                ChatForm.chat.append(user + "\n");
            }
        }
        ChatForm.chat.append("\n");
    }
}
