package APPOO.CourseThesis.ui;

import java.awt.*;
import javax.swing.*;

/**
 * Created by Dennis on 18-Jun-17.
 **/
public class IChat {

private static void createAndShowGUI() {

        JFrame app = new JFrame();

        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setLayout(new BorderLayout());
        app.setVisible(true);

        //Size config
        app.setPreferredSize(new Dimension(400, 85));
        app.setResizable(false);

        new LoginForm(app);

        app.pack();
        app.setLocationRelativeTo(null);
        app.setVisible(true);
    }

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException|IllegalAccessException|InstantiationException|ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        javax.swing.SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

}
