package APPOO.CourseThesis.server;

import APPOO.CourseThesis.tools.Constants;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dennis on 24-May-17.
 **/
public class MultiServerThread extends Thread {

    private Socket socket = null;
    private PrintWriter out;
    private String nickname;

    public MultiServerThread(Socket socket) {
        super("MultiServerThread");
        this.socket = socket;
    }

    public void run() {

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String fromClient;
            try {
                while (true) {

                    fromClient = in.readLine();
                    if (fromClient != null) {

                        List<String> clientPackage = Arrays.asList(fromClient.split(Constants.Client.SEPARATOR));

                        if (clientPackage.get(0).equals(Constants.Client.EXIT_COMMAND)) {
                            stopSession();
                            break;
                        }
                        else if (clientPackage.get(0).equals(Constants.Client.LIST_COMMAND)) {
                            String users = getOnlineUsers();
                            out.println(users);
                        }
                        else if (clientPackage.get(0).equals(Constants.Client.NEW_USER)) {
                            validateNickname(clientPackage.get(1));
                        }
                        else if (clientPackage.get(0).equals(Constants.Client.PRIVATE_MESSAGE_COMMAND)) {
                            if (clientPackage.size() == 2) {
                                validatePMessageNickname(clientPackage.get(1));
                            }
                            else {
                                for (MultiServerThread multiServerThread: Server.multiServerThreads) {
                                    if (multiServerThread.getNickname().equals(clientPackage.get(1))) {

                                        multiServerThread.sendToClient(Constants.Server.PM + nickname, clientPackage.get(2));
                                    }
                                }
                            }
                        }
                        else {

                            if (Server.multiServerThreads.size() > 1) {

                                for (MultiServerThread multiServerThread: Server.multiServerThreads) {
                                    if (!multiServerThread.getNickname().equals(null)) {
                                        if (!multiServerThread.getNickname().equals(nickname)) {

                                            multiServerThread.sendToClient(nickname, clientPackage.get(1));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
                removeThread();
            }

            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void validateNickname(String nickname) {

        if(Server.listOfUsers.size() == 0) {
            System.out.println("User " + nickname + " joined the chat.");
            Server.listOfUsers.add(nickname);
            this.nickname = nickname;
            out.println(Constants.Client.NEW_USER + Constants.Client.SEPARATOR + "true");
        }

        else {
            if (Server.listOfUsers.contains(nickname)) {
                System.out.println("Invalid username " + nickname + ".");
                out.println(Constants.Client.NEW_USER + Constants.Client.SEPARATOR + "false");
            }
            else {
                System.out.println("User " + nickname + " joined the chat.");

                Server.listOfUsers.add(nickname);
                this.nickname = nickname;
                out.println(Constants.Client.NEW_USER + Constants.Client.SEPARATOR + "true");
                System.out.println("Sa-mi bag pula " + this.getNickname());
            }
        }
    }

    private void validatePMessageNickname(String nickname) {
        if(Server.listOfUsers.size() > 1) {

            if (Server.listOfUsers.contains(nickname)) {
                out.println(Constants.Client.PRIVATE_MESSAGE_COMMAND + Constants.Client.SEPARATOR + "true");
                return;
            }
        }

        out.println(Constants.Client.PRIVATE_MESSAGE_COMMAND + Constants.Client.SEPARATOR + "false");
    }

    private void removeThread() {

        System.out.println("User " + nickname + " unexpectedly disconnected.");

        if(Server.listOfUsers.contains(nickname)) {
            Server.listOfUsers.remove(nickname);
        }
        Server.multiServerThreads.remove(this);
    }

    private String getOnlineUsers() {

        String users = "";
        for (String user: Server.listOfUsers) {
            users = users + user + Constants.Client.SEPARATOR;
        }

        return Constants.Client.LIST_COMMAND + Constants.Client.SEPARATOR + users;
    }

    private void sendToClient(String senderNickname, String message) {

        out.println(Constants.Client.MESSAGE_COMMAND + Constants.Client.SEPARATOR + senderNickname + ": " + message);
    }

    private String getNickname() {
        return nickname;
    }

    private void stopSession() {

        System.out.println("User " + nickname + " left the chat.");

        Server.listOfUsers.remove(nickname);
        Server.multiServerThreads.remove(this);
    }
}
